# Contenu
* Requêtage sur base MySQL en PHP (utilisation de la bibliotèque PHP dépréciée `mysql`)
* Présentation des informations en mode TUI (console)
* Présentation des informations en mode GUI (navigateur HTML)
* Transfert de variables par HTTP (formulaire et méthode POST)
* Utilisation des variables HTTP
* Mise en œuvre du mécanisme de session PHP

# Objectifs
*  Architectures applicatives : concepts de base
* Techniques de présentation des données et des documents
* Interfaces homme-machine
* Concevoir une interface utilisateur
* Développer et maintenir une application exploitant une base de données partagée

# Compétences
* D1.1 -- Analyse de la demande
* D4.1 -- Conception et réalisation d’une solution applicative
 * A4.1.2 Conception ou adaptation de l’interface utilisateur d’une solution applicative
 * A4.1.7 Développement, utilisation ou adaptation de composants logiciels
 * A4.1.8 Réalisation des tests nécessaires à la validation d’éléments adaptés ou développés
